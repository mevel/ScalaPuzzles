lazy val root = (project in file (".")).
settings (
    name := "ScalaPuzzles",
    scalaVersion := "2.12.6",
    libraryDependencies += "org.scala-lang.modules" %% "scala-swing" % "2.1.1"
)
